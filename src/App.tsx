import React from 'react';
import logo from './logo.svg';
import {
  HashRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import './App.css';
import 'materialize-css/dist/css/materialize.min.css';
import Home from './components/home/home';
import LogIn from './components/login/login.js'

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login" component={LogIn} />
        <Route path="/home" component={Home} />
        <Redirect from='/' to='/login' />
      </Switch>
    </Router>
  );
}

export default App;
