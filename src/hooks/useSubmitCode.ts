import { useEffect, useState } from "react";
import { usePost } from "./usePost";

export function useSubmitCode(codeToSubmit: string) {

    const [ code, setCode] = useState('');

    const { response, error, state } = usePost('asd.com', code);

    useEffect(() => {
        if(codeToSubmit) {
            setCode(codeToSubmit);
        }
    }, [codeToSubmit]);

    return { response, error, state };
}