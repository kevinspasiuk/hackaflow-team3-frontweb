import axios, { AxiosResponse } from 'axios';
import { stringify } from 'node:querystring';
import { useState, useEffect } from 'react'

interface UsePostResponse<T> {
    response: T | undefined;
    error: any,
    state: GetState
}

function MockResponse(): Promise<IMockResponse> {
  return Promise.resolve({
    data: 'Esto anduvo'
  });
}

interface IMockResponse {
  data: string;
}

export type GetState = 'ready' | 'loading' | 'error'; 

export const usePost = <T, R>(url: string, data: T) : UsePostResponse<R> => {
    //const [response, setResponse] = useState<R | undefined>(undefined);
    const [response, setResponse] = useState<any>(undefined);
    const [error, setError] = useState(null);
    const [state, setStatus] = useState<GetState>('loading');
    
    useEffect(() => {
      const fetchData = async () => {
        if (data) {
          try {
            console.log(data);
            setStatus('loading');
            //const res: AxiosResponse<R> = await axios.post<R>(url, data);
            const res = await MockResponse();
            setResponse(res);
            setStatus('ready');
          } catch (error) {
            setStatus('error');
            setError(error);
          }          
        }
      };
      fetchData();
    }, [data]);
    
    return { response, error, state };
};