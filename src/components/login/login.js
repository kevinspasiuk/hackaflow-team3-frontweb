import React from 'react';
import M from 'materialize-css';
import {Link} from "react-router-dom";

import './login.css';

function LogIn(props) {

  M.updateTextFields();

  return (
    <div className="container-fluid bg-img-login">
      <div className="row">
        <div className="col s12">
          <img src="https://web.flow.com.ar/theme/flow/logo.svg" className="img-top" />
        </div>
      </div>
      <div id="login-page" className="row">
        <div className="col s12 z-depth-6 card-panel">
          <form className="login-form">

            <div className="row">
            </div>

            <div className="row">
              <div className="input-field col s12">
                <input className="validate" id="email" type="email" />
                <label htmlFor="email" data-error="wrong" data-success="right">Email</label>
              </div>
            </div>

            <div className="row">
              <div className="input-field col s12">
                <input className="validate" id="password" type="password" />
                <label htmlFor="password">Password</label>
              </div>
            </div>

            <div className="row">
              <div className="input-field col s12">
                <Link to="/home" className="btn waves-effect waves-light col s12">Login</Link>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  );
}

export default LogIn;
