import React from 'react';
import M from 'materialize-css';
import {Link} from "react-router-dom";

import './navbar.css';

function NavBar(props) {
  M.AutoInit();
  let elems = document.querySelectorAll('.dropdown-trigger');
  M.Dropdown.init(elems, {inDuration: 300, outDuration: 225});
  return (
    <nav>
      <div className="nav-wrapper">
        <a href="#" className="brand">
          <img src="https://web.flow.com.ar/theme/flow/logo.svg" className="logo-flow" />
        </a>
        <ul id="nav-mobile" className="right hide-on-med-and-down">
          <li><a href="#">Inicio</a></li>
          <li><a href="#">Guia TV</a></li>
          <li><a href="#">Peliculas</a></li>
          <li><a href="#">Series</a></li>
          <li>
            <a href="#">
              <img src="https://web.flow.com.ar/img/uikit/navbar/kids.svg" className="infantiles" />
            </a>
          </li>
          <li>
            <a className="dropdown-trigger" href="#!" data-target="dropdown1">
              <img src="https://web.flow.com.ar/img/uikit/avatar/1_focus.svg" className="avatar" />
            </a>
            <ul id="dropdown1" className="dropdown-content dropflow">
              <li><a href="#!">Pato</a></li>
              <li><a href="#!">Perfiles</a></li>
              <li><a href="#!">Mi Cuenta</a></li>
              <li><a href="#!">Control Parental</a></li>
              <li className="divider"></li>
              <li><a href="#!">Aplicación de Escritorio</a></li>
              <li><a href="#!">Ayuda</a></li>
              <li><Link to="/">Cerrar sesión</Link></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default NavBar;
