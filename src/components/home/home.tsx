import { useCallback, useState } from 'react';
import NavBar from '../navbar/navbar.js'
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import './home.css';
import { useSubmitCode } from '../../hooks/useSubmitCode';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

function Home() {

  const [code, setCode] = useState<string>('');
  const [codeToSubmit, setCodeToSubmit] = useState<string>('');

  const { response, error, state } = useSubmitCode(codeToSubmit);

  let content = (<div></div>);

  if (!codeToSubmit) {
    content = (
      <Grid   
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{ minHeight: '90vh' }}>
          <Typography variant="h3" component="h2">
            Introduzca el código
          </Typography>
          <div className="input-field col s12">
            <input 
              onChange={e => setCode(e.target.value)}
              className="code-input" id="password" type="text"
              autoComplete="off" />
          </div>
          <button 
            disabled={!code}
            onClick={ () => setCodeToSubmit(code)} 
            className="btn waves-effect waves-light" >Aceptar</button>
      </Grid>
    );
  } else if (state === 'ready') {
    content = (
      <Grid   
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{ minHeight: '90vh' }}>
          <Typography variant="h4" component="h2">
            Se ha vinculado con éxito
          </Typography>
          <CheckCircleIcon style={ {fontSize: '8em', color: 'green' }} />
      </Grid>
    );
  } else if (state === 'error') {
    content = (
      <div>
        Error en la conexión.
      </div>
    );
  }

  return (
    <div className="container-fluid">
      <NavBar/>

      <Container maxWidth="md">
        {content}
      </Container>
    </div>
  );
}

export default Home;
